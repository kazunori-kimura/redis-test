package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class AppConfig {

	private static Logger logger = LogManager.getLogger(AppConfig.class);

	private static AppConfig instance = new AppConfig();

	private String server;
	private int port;
	private int threadCount;
	private int interval;
	private long itemCount;

	private AppConfig() {
		this.load();
	}

	public static AppConfig getInstance() {
		return instance;
	}

	/**
	 * config.propertiesの読み込み
	 */
	private void load() {
		logger.traceEntry();
		InputStream stream = null;
		try {
			stream = getClass().getClassLoader().getResourceAsStream("config.properties");
			Properties props = new Properties();
			props.load(stream);

			this.server = props.getProperty("server");
			this.port = Integer.parseInt(props.getProperty("port"));
			this.threadCount = Integer.parseInt(props.getProperty("thread-count"));
			this.interval = Integer.parseInt(props.getProperty("interval"));
			this.itemCount = Long.parseLong(props.getProperty("item-count"));

			logger.info(String.format("スレッド数: %d 個", this.threadCount));
			logger.info(String.format("登録件数: %d 件/スレッド", this.itemCount));
			logger.info(String.format("実行間隔: %d ms/件", this.interval));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {} //例外は無視
			}
		}

		logger.traceExit();
	}

	public String getServer() {
		return server;
	}
	public int getPort() {
		return port;
	}
	public int getThreadCount() {
		return threadCount;
	}
	public int getInterval() {
		return interval;
	}
	public long getItemCount() {
		return itemCount;
	}
}
