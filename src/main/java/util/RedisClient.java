package util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public final class RedisClient {
	private static RedisClient instance = new RedisClient();
	
	private static Logger logger = LogManager.getLogger(RedisClient.class);

	private JedisPool pool;

	private RedisClient() {
	}

	public static RedisClient getInstance() {
		return instance;
	}

	/**
	 * Redisへの接続を返します
	 *
	 * @return
	 */
	public Jedis getConnection() {
		logger.traceEntry();
		
		synchronized (this) {
			if (this.pool == null || this.pool.isClosed()) {
				this.pool = new JedisPool(new JedisPoolConfig(), AppConfig.getInstance().getServer(),
						AppConfig.getInstance().getPort());
			}
		}

		return this.pool.getResource();
	}

	/**
	 * JedisPoolを破棄します
	 */
	public void close() {
		logger.traceEntry();
		
		if (this.pool != null) {
			this.pool.destroy();
		}
	}
}
