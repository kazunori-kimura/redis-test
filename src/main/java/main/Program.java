package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.DataRegister;
import redis.clients.jedis.Jedis;
import util.AppConfig;
import util.RedisClient;

public class Program {

	private static Logger logger = LogManager.getLogger(Program.class);

	private int threadCount;

	private List<Thread> threads;

	public static void main(String[] args) {
		logger.info("start main thread.");

		// Shutdown時に呼び出される処理
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			logger.info("start shutdown hook.");
			// RedisPoolの破棄
			RedisClient.getInstance().close();
		}));

		// 送信処理開始
		Program p = new Program();
		p.start();

		// 完了するまで待つ
		while(p.isAlive()) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// 件数を取得
		p.report();

		logger.info("end main thread.");
	}


	public Program() {}

	/**
	 * Thread起動
	 */
	public void start() {
		logger.traceEntry();
		this.threadCount = AppConfig.getInstance().getThreadCount();
		this.threads = new ArrayList<Thread>();

		for (int i=1; i<=this.threadCount; i++) {
			DataRegister task = new DataRegister(i);

			// スレッド実行
			Thread t = new Thread(task);
			t.setName(String.format("thread_%d", i));
			t.start();

			// スレッドを保持する
			this.threads.add(t);

			// 30msくらい間隔を空けてみる
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		logger.traceExit();
	}

	/**
	 * 登録件数を表示する
	 */
	public void report() {
		logger.traceEntry();

		try (Jedis jedis = RedisClient.getInstance().getConnection()) {
			// キーを取得
			Set<String> keys = jedis.keys("thread:*");

			for (String key : keys) {
				// キーに登録されている項目数を取得
				long count = jedis.zcount(key, "-inf", "+inf");

				logger.info(String.format("Key:%s ,登録件数:%d", key, count));
			}
		}

		logger.traceExit();
	}

	/**
	 * Threadの生存確認
	 *
	 * @return
	 */
	public boolean isAlive() {
		logger.traceEntry();
		boolean ret = false;

		// ひとつでも残っているThreadがあればtrue
		for (Thread t : this.threads) {
			if (t.isAlive()) {
				ret = true;
				break;
			}
		}

		return logger.traceExit(ret);
	}

}
