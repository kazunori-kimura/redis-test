package core;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import redis.clients.jedis.Jedis;
import util.AppConfig;
import util.RedisClient;

/**
 * Redis登録スレッド
 *
 * @author kimki
 *
 */
public class DataRegister implements Runnable {

	private static Logger logger = LogManager.getLogger(DataRegister.class);

	private int threadId;
	private int interval;
	private long maxCount;
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");

	public DataRegister(int id) {
		this.threadId = id;
		this.interval = AppConfig.getInstance().getInterval();
		this.maxCount = AppConfig.getInstance().getItemCount();
	}

	/**
	 * 送信を繰り返す
	 */
	@Override
	public void run() {
		String key = this.getKey();
		logger.info(String.format("start %s", key));

		// 指定回数繰り返す
		for (long i=0; i<this.maxCount; i++) {
			Date date = new Date();
			String value = this.getValue(i, date);
			long score = date.getTime();

			// Redis登録処理
			this.register(key, score, value);

			try {
				// 指定期間停止
				Thread.sleep(this.interval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		logger.info(String.format("end %s", key));
	}

	/**
	 * 登録
	 *
	 * @param key
	 * @param score
	 * @param value
	 */
	private void register(String key, long score, String value) {
		logger.traceEntry();

		try (Jedis jedis = RedisClient.getInstance().getConnection()) {
			jedis.zadd(key, score, value);
		}

		logger.traceExit();
	}

	/**
	 * キー
	 *
	 * @return
	 */
	private String getKey() {
		String key = String.format("thread:%d", this.threadId);
		return logger.traceExit(key);
	}

	/**
	 * 値
	 *
	 * @param count
	 * @param date
	 * @return
	 */
	private String getValue(long count, Date date) {
		String value = String.format("count:%s,date:%s", count, this.formatter.format(date));
		return logger.traceExit(value);
	}

}
